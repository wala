/*******************************************************************************
 * Copyright (c) 2002 - 2006 IBM Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.ibm.wala.util.tables;

/**
 * 
 * Misc. constants used to define Tables 
 * 
 * @author sfink
 */
public class Constants {
  
  /**
   * Any line that begins with this character is treated as a comment
   */
  public static final char COMMENT = '#';

}
