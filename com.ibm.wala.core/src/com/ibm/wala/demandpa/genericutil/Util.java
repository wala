/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipientís reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regentsí employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package com.ibm.wala.demandpa.genericutil;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.security.Permission;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.ibm.wala.util.collections.HashSetFactory;

/**
 * Miscellaneous utility functions.
 */
public class Util {

  /** The empty {@link BitSet}. */
  public static final BitSet EMPTY_BITSET = new BitSet();

  /** Convert an int[] to a {@link String} for printing 
   * @throws IllegalArgumentException  if ints == null*/
  public static String str(int[] ints) throws IllegalArgumentException {
    if (ints == null) {
      throw new IllegalArgumentException("ints == null");
    }
    StringBuffer s = new StringBuffer();
    s.append("[");
    for (int i = 0; i < ints.length; i++) {
      if (i > 0)
        s.append(", ");
      s.append(ints[i]);
    }
    s.append("]");
    return s.toString();
  }

  public static String objArrayToString(Object[] o) {
    return objArrayToString(o, "[", "]", ", ");
  }

  public static String objArrayToString(Object[] o, String start, String end, String sep) throws IllegalArgumentException {
    if (o == null) {
      throw new IllegalArgumentException("o == null");
    }
    StringBuffer s = new StringBuffer();
    s.append(start);
    for (int i = 0; i < o.length; i++) {
      if (o[i] != null) {
        if (i > 0)
          s.append(sep);
        s.append(o[i].toString());
      }
    }
    s.append(end);
    return s.toString();
  }

  /** Get a {@link String} representation of a {@link Throwable}. 
   * @throws IllegalArgumentException  if thrown == null*/
  public static String str(Throwable thrown) throws IllegalArgumentException {
    if (thrown == null) {
      throw new IllegalArgumentException("thrown == null");
    }
    // create a memory buffer to which to dump the trace
    ByteArrayOutputStream traceDump = new ByteArrayOutputStream();
    PrintWriter w = new PrintWriter(traceDump);
    thrown.printStackTrace(w);
    w.close();
    return traceDump.toString();
  }

  /**
   * Test whether <em>some</em> element of the given {@link Collection}
   * satisfies the given {@link Predicate}.
   * @throws IllegalArgumentException  if c == null
   */
  public static <T> boolean forSome(Collection<T> c, Predicate<T> p) throws IllegalArgumentException {
    if (c == null) {
      throw new IllegalArgumentException("c == null");
    }
    for (T t : c) {
      if (p.test(t)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Test whether <em>some</em> element of the given {@link Collection}
   * satisfies the given {@link Predicate}.
   * 
   * @return The first element satisfying the predicate; otherwise null.
   * @throws IllegalArgumentException  if c == null
   */
  public static <T> T find(Collection<T> c, Predicate<T> p) throws IllegalArgumentException {
    if (c == null) {
      throw new IllegalArgumentException("c == null");
    }
    for (Iterator<T> iter = c.iterator(); iter.hasNext();) {
      T obj = iter.next();
      if (p.test(obj))
        return obj;
    }

    return null;
  }

  /**
   * Test whether <em>some</em> element of the given {@link Collection}
   * satisfies the given {@link Predicate}.
   * 
   * @return All the elements satisfying the predicate
   * @throws IllegalArgumentException  if c == null
   */
  public static <T> Collection<T> findAll(Collection<T> c, Predicate<T> p) throws IllegalArgumentException {
    if (c == null) {
      throw new IllegalArgumentException("c == null");
    }
    Collection<T> result = new LinkedList<T>();

    for (Iterator<T> iter = c.iterator(); iter.hasNext();) {
      T obj = iter.next();
      if (p.test(obj))
        result.add(obj);
    }

    return result;
  }

  /**
   * Test whether <em>all</em> elements of the given {@link Collection}
   * satisfy the given {@link Predicate}.
   * @throws NullPointerException  if c == null
   */
  public static <T> boolean forAll(Collection<T> c, Predicate<T> p) throws NullPointerException {
    for (T t : c) {
      if (!p.test(t))
        return false;
    }
    return true;
  }

  /**
   * Perform an action for all elements in a collection.
   * 
   * @param c
   *            the collection
   * @param v
   *            the visitor defining the action
   * @throws IllegalArgumentException  if c == null
   */
  public static <T> void doForAll(Collection<T> c, ObjectVisitor<T> v) throws IllegalArgumentException {
    if (c == null) {
      throw new IllegalArgumentException("c == null");
    }
    for (Iterator<T> iter = c.iterator(); iter.hasNext();)
      v.visit(iter.next());
  }

  /**
   * Map a list: generate a new list with each element mapped. The new list is
   * always an {@link ArrayList}; it would have been more precise to use
   * {@link java.lang.reflect reflection} to create a list of the same type as
   * 'srcList', but reflection works really slowly in some implementations, so
   * it's best to avoid it.
   * 
   * @throws IllegalArgumentException
   *             if srcList == null
   */
  public static <T, U> List<U> map(List<T> srcList, Mapper<T, U> mapper) throws IllegalArgumentException {
    if (srcList == null) {
      throw new IllegalArgumentException("srcList == null");
    }
    ArrayList<U> result = new ArrayList<U>();
    for (Iterator<T> srcIter = srcList.iterator(); srcIter.hasNext();) {
      result.add(mapper.map(srcIter.next()));
    }
    return result;
  }

  /**
   * Filter a collection: generate a new list from an existing collection,
   * consisting of the elements satisfying some predicate. The new list is
   * always an {@link ArrayList}; it would have been more precise to use
   * {@link java.lang.reflect reflection} to create a list of the same type as
   * 'srcList', but reflection works really slowly in some implementations, so
   * it's best to avoid it.
   * @throws IllegalArgumentException  if src == null
   */
  public static <T> List<T> filter(Collection<T> src, Predicate<T> pred) throws IllegalArgumentException {
    if (src == null) {
      throw new IllegalArgumentException("src == null");
    }
    ArrayList<T> result = new ArrayList<T>();
    for (Iterator<T> srcIter = src.iterator(); srcIter.hasNext();) {
      T curElem = srcIter.next();
      if (pred.test(curElem))
        result.add(curElem);
    }
    return result;
  }

  /**
   * Filter a collection according to some predicate, placing the result in a
   * List
   * 
   * @param src
   *            collection to be filtered
   * @param pred
   *            the predicate
   * @param result
   *            the list for the result. assumed to be empty
   * @throws IllegalArgumentException  if src == null
   */
  public static <T> void filter(Collection<T> src, Predicate<T> pred, List<T> result) throws IllegalArgumentException {
    if (src == null) {
      throw new IllegalArgumentException("src == null");
    }
    for (T t : src) {
      if (pred.test(t)) {
        result.add(t);
      }
    }
  }

  /**
   * Map a set: generate a new set with each element mapped. The new set is
   * always a {@link HashSet}; it would have been more precise to use
   * {@link java.lang.reflect reflection} to create a set of the same type as
   * 'srcSet', but reflection works really slowly in some implementations, so
   * it's best to avoid it.
   * @throws IllegalArgumentException  if srcSet == null
   */
  public static <T, U> Set<U> mapToSet(Collection<T> srcSet, Mapper<T, U> mapper) throws IllegalArgumentException {
    if (srcSet == null) {
      throw new IllegalArgumentException("srcSet == null");
    }
    HashSet<U> result = HashSetFactory.make();
    for (Iterator<T> srcIter = srcSet.iterator(); srcIter.hasNext();) {
      result.add(mapper.map(srcIter.next()));
    }
    return result;
  }

  /*
   * Grow an int[] -- i.e. allocate a new array of the given size, with the
   * initial segment equal to this int[].
   */
  public static int[] realloc(int[] data, int newSize) throws IllegalArgumentException {
    if (data == null) {
      throw new IllegalArgumentException("data == null");
    }
    if (data.length < newSize) {
      int[] newData = new int[newSize];
      System.arraycopy(data, 0, newData, 0, data.length);
      return newData;
    } else
      return data;
  }

  /** Clear a {@link BitSet}. 
   * @throws IllegalArgumentException  if bitSet == null*/
  public static void clear(BitSet bitSet) throws IllegalArgumentException {
    if (bitSet == null) {
      throw new IllegalArgumentException("bitSet == null");
    }
    bitSet.and(EMPTY_BITSET);
  }

  /** Replace all occurrences of a given substring in a given {@link String}. 
   * @throws IllegalArgumentException  if str == null*/
  public static String replaceAll(String str, String sub, String newSub) throws IllegalArgumentException {
    if (str == null) {
      throw new IllegalArgumentException("str == null");
    }
    if (str.indexOf(sub) == -1) {
      return str;
    }
    int subLen = sub.length();
    int idx;
    StringBuffer result = new StringBuffer(str);
    while ((idx = result.toString().indexOf(sub)) >= 0)
      result.replace(idx, idx + subLen, newSub);
    return result.toString();
  }

  /** Remove all occurrences of a given substring in a given {@link String} */
  public static String removeAll(String str, String sub) {
    return replaceAll(str, sub, "");
  }

  /** Generate strings with fully qualified names or not */
  public static final boolean FULLY_QUALIFIED_NAMES = false;

  /**
   * Write object fields to string
   * 
   * @throws IllegalArgumentException
   *             if obj == null
   */
  public static String objectFieldsToString(Object obj) throws IllegalArgumentException {
    if (obj == null) {
      throw new IllegalArgumentException("obj == null");
    }
    // Temporarily disable the security manager
    SecurityManager oldsecurity = System.getSecurityManager();
    System.setSecurityManager(new SecurityManager() {
      @Override
      public void checkPermission(Permission perm) {
      }
    });

    Class c = obj.getClass();
    StringBuffer buf = new StringBuffer(FULLY_QUALIFIED_NAMES ? c.getName() : removePackageName(c.getName()));
    while (c != Object.class) {
      Field[] fields = c.getDeclaredFields();

      if (fields.length > 0)
        buf = buf.append(" (");

      for (int i = 0; i < fields.length; i++) {
        // Make this field accessible
        fields[i].setAccessible(true);

        try {
          Class type = fields[i].getType();
          String name = fields[i].getName();
          Object value = fields[i].get(obj);

          // name=value : type
          buf = buf.append(name);
          buf = buf.append("=");
          buf = buf.append(value == null ? "null" : value.toString());
          buf = buf.append(" : ");
          buf = buf.append(FULLY_QUALIFIED_NAMES ? type.getName() : removePackageName(type.getName()));
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        }

        buf = buf.append(i + 1 >= fields.length ? ")" : ",");
      }
      c = c.getSuperclass();
    }
    // Reinstate the security manager
    System.setSecurityManager(oldsecurity);

    return buf.toString();
  }

  /** Remove the package name from a fully qualified class name */
  public static String removePackageName(String fully_qualified_name_) {
    if (fully_qualified_name_ == null)
      return null;

    int lastdot = fully_qualified_name_.lastIndexOf('.');

    if (lastdot < 0) {
      return "";
    } else {
      return fully_qualified_name_.substring(lastdot + 1);
    }
  }

  /**
   * @return a hash code for the array
   * @throws IllegalArgumentException
   *             if objs == null
   */
  public static int hashArray(Object[] objs) throws IllegalArgumentException {
    if (objs == null) {
      throw new IllegalArgumentException("objs == null");
    }
    // stolen from java.util.AbstractList
    int ret = 1;
    for (int i = 0; i < objs.length; i++) {
      ret = 31 * ret + (objs[i] == null ? 0 : objs[i].hashCode());
    }
    return ret;

  }

  public static boolean arrayContains(Object[] arr, Object obj, int size) {
    assert obj != null;
    for (int i = 0; i < size; i++) {
      if (arr[i] != null && arr[i].equals(obj))
        return true;
    }
    return false;
  }

  public static String toStringNull(Object o) {
    return o == null ? "" : "[" + o.toString() + "]";
  }

  /**
   * checks if two sets have a non-empty intersection
   * 
   * @param s1
   * @param s2
   * @return <code>true</code> if the sets intersect; <code>false</code>
   *         otherwise
   */
  public static <T> boolean intersecting(final Set<T> s1, final Set<T> s2) {
    return forSome(s1, new Predicate<T>() {
      @Override
      public boolean test(T obj) {
        return s2.contains(obj);
      }
    });
  }

  public static int getInt(Integer i) {
    return (i == null) ? 0 : i;
  }

  /**
   * given the name of a class C, returns the name of the top-most enclosing
   * class of class C. For example, given A$B$C, the method returns A
   * 
   * @return String name of top-most enclosing class
   * @throws IllegalArgumentException  if typeStr == null
   */
  public static String topLevelTypeString(String typeStr) throws IllegalArgumentException {
    if (typeStr == null) {
      throw new IllegalArgumentException("typeStr == null");
    }
    int dollarIndex = typeStr.indexOf('$');
    String topLevelTypeStr = dollarIndex == -1 ? typeStr : typeStr.substring(0, dollarIndex);
    return topLevelTypeStr;
  }

  public static <T> void addIfNotNull(T val, Collection<T> vals) {
    if (val != null) {
      vals.add(val);
    }
  }

  public static <T> List<T> pickNAtRandom(List<T> vals, int n, long seed) throws IllegalArgumentException {
    if (vals == null) {
      throw new IllegalArgumentException("vals == null");
    }
    if (vals.size() <= n) {
      return vals;
    }
    HashSet<T> elems = HashSetFactory.make();
    Random rand = new Random(seed);
    for (int i = 0; i < n; i++) {
      boolean added = true;
      do {
        int randIndex = rand.nextInt(n);
        added = elems.add(vals.get(randIndex));
      } while (!added);

    }
    return new ArrayList<T>(elems);
  }
} // class Util
